<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use Spatie\FlareClient\View;
use App\Http\Controllers\castcontroller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'Home']);

Route::get('/Register', [HomeController::class,'Reg']);

Route::post('/helo', [HomeController::class,'halo']);

Route::get('/table',function(){
    return view('page.table');
});

Route::get('/data-table',function(){
    return view('page.data-table');
});

//CRUD
//Create
//form cast
Route::get('cast/create', [castcontroller::class, 'create']);
//kirim db cast
Route::post('/cast',[castcontroller::class,'store']);

//Read
//tampil
Route::get('/cast', [castcontroller::class, 'index']);
//Detail cast
Route::get('/cast/{cast_id}', [castcontroller::class, 'show']);

//update
//form update
Route::get('/cast/{cast_id}/edit', [castcontroller::class, 'edit']);
//update ke database berdasar id
Route::put('/cast/{cast_id}', [castcontroller::class, 'update']);

//delete
//delete berdasar id
Route::delete('/cast/{cast_id}', [castcontroller::class, 'destroy']);