<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function Home(){
        return view('Home');
    }

    public function Reg(){
        return view('Register');
    }
    
    public function halo(Request $rq)
    {
        $nameDepan = $rq->input('nameF');
        $nameBelakang = $rq->input('nameL');

        return view ('Halo', ['nameDepan' => $nameDepan , 'nameBelakang' => $nameBelakang]);
    }
}