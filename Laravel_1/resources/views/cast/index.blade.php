@extends('layout.master')

@section('judul')
Page List Cast
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm">Tambah</a>
<table class="table table-striped table-dark">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $hasil)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$hasil->nama}}</td>
                <td>{{$hasil->umur}}</td>
                <td>
                    <form action="/cast/{{$hasil->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/cast/{{$hasil->id}}" class="btn btn-primary btn-sm">Detail</a>
                    <a href="/cast/{{$hasil->id}}/edit" class="btn btn-success btn-sm">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    </form>
                </td>  
            </tr>
        @empty
            <tr>
                <td>Tidak ada Data</td>
            </tr>
        @endforelse
    </tbody>
  </table>
@endsection