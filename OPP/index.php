<?php

    // require ('animal.php');
    //require ('Frog.php');
    require ('Ape.php');
    
    $sheep = new animal ("shaun");

    echo "Name :  $sheep->name  <br>";
    echo "Legs :  $sheep->legs  <br>";
    echo "cold_blooded :  $sheep->cold_blooded  <br>";

    echo "<br>";

    $kodok = new frog ("buduk");

    echo "Name :  $kodok->name  <br>";
    echo "Legs :  $kodok->legs  <br>";
    echo "cold_blooded :  $kodok->cold_blooded  <br>";
    echo "Jump :  $kodok->jump  <br>";

    echo "<br>";

    $sungokong = new ape ("Kera sakti");

    echo "Name :  $sungokong->name  <br>";
    echo "Legs :  $sungokong->legs  <br>";
    echo "cold_blooded :  $sungokong->cold_blooded  <br>";
    echo "Jump :  $sungokong->yell  <br>";
?>