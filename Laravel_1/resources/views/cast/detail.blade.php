@extends('layout.master')

@section('judul')
Page Detail Cast
@endsection

@section('content')
    <h2>Nama : {{$cast->nama}}</h2>
    <p>Dengan Umur : {{$cast->umur}}</p>
    <p>{{$cast->bio}}</p>

    <a href="/cast" class="btn btn-dark btn-sm">Back</a>
@endsection