@extends('layout.master')

@section('judul')
Buat Account Baru!
@endsection

@section('content')
<h3>Sing Up Form</h3>
<form action="/helo" method="POST">
    @csrf
    <label>First Name :</label><br>
    <input type="text" name="nameF"><br><br>
    <label>Last Name :</label><br>
    <input type="text" name="nameL"><br><br>
    <label>Gender :</label><br>
    <input type="radio" id="Male" name="Male" value="Male">
    <label for="Male">Male</label><br>
    <input type="radio" id="Female" name="Female" value="Female">
    <label for="Female">Female</label><br>
    <input type="radio" id="other" name="other" value="other">
    <label for="other">other</label><br><br>
    <label>Nasionality</label><br>
    <select name="negara">
        <option value="Indonesia">Indonesia</option>
        <option value="Malaysia">Malaysia</option>
        <option value="Singapura">Singapura</option>
        <option value="Vietnam">Vietnam</option>
        <option value="Thailand">Thailand</option>
        <option value="Australia">Australia</option>
        <option value="Irak">Irak</option>
    </select><br><br>
    <label>Language Spoken :</label><br>
    <input type="checkbox" id="Indonesia" name="Indonesia" value="Indonesia">
    <label for="Indonesia">Indonesia</label><br>
    <input type="checkbox" id="Inggris" name="Inggris" value="Inggris">
    <label for="Inggris">Inggris</label><br>
    <input type="checkbox" id="Other" name="Other" value="Other">
    <label for="Other">Other</label><br><br>
    <label>Bio :</label><br>
    <textarea name="biografi" rows="5" cols="30"></textarea><br><br>
    <input type="submit" value="Sign Up">
</form>

@endsection
    